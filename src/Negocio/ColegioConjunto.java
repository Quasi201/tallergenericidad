/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Util.Conjunto;

/**
 *
 * @author quasi201
 */
public class ColegioConjunto {
    private String nombreColegio;
    private Conjunto<Estudiante> estudiantes;
    
    public ColegioConjunto(){}
    
    public ColegioConjunto(String nombreColegio, int cantEstudiantes){
        this.nombreColegio=nombreColegio;
        this.estudiantes = new Conjunto(cantEstudiantes);
    }
    
    public void matricular(Estudiante nuevo) throws Exception{
        this.estudiantes.adicionarElemento(nuevo);
    }
    
    public boolean existeEstudiante(Estudiante nuevo){
        return this.estudiantes.existeElemento(nuevo);
    }
    
    public int getDondeEsta(Estudiante e){
        return estudiantes.indexOf(e);
    }
    
    public void desMatricular(Estudiante x)throws Exception{
        this.estudiantes.remover(x);
    }
    
    @Override
    public String toString(){
        String msg="Colegio: "+this.nombreColegio+"\n";
        msg+=this.estudiantes.toString();
        return msg;
    }
    
    public String getListadoNombres(){
        String msg="";
        for (int i = 0; i <estudiantes.getCapacidad(); i++) {
            msg+=estudiantes.get(i).getNombre()+"\n";
        }
        return msg;
    }
}
