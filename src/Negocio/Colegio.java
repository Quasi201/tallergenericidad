/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Util.Caja;

/**
 *
 * @author quasi201
 */
public class Colegio {
    private String nombre;
    Caja<Estudiante> estudiantes[];
    private int i=0;
    
    public Colegio(){
        
    }
    
    public Colegio(String nombre, int cantEstudiantes) throws Exception{
        if(cantEstudiantes < 0)throw new Exception("No se pueden crear los espacios para los estudiantes");
        this.nombre = nombre;
        estudiantes = new Caja[cantEstudiantes];
    }
    
    public void matricular(Estudiante nuevo)throws Exception{
        if(i>=estudiantes.length)throw new Exception("No hay espacio para el estudiante");
        this.estudiantes[i]=new Caja(nuevo);
        i++;
    }
    
    private boolean existeEstudiante(Estudiante nuevo){
        for (int j = 0; j<i; j++) {
            Estudiante x = this.estudiantes[j].getObjeto();
            if(x.equals(nuevo)) return true;
        }
        return false;
    }
            
    public int getMatriculados(){
        return this.i;
    }
    
    @Override
    public String toString(){
        String msg=this.nombre+"\n";
        for (Caja  c : this.estudiantes) {
            msg+=c.getObjeto().toString()+"\n";
        }
        return msg;
    }
}
