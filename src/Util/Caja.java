/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author quasi201
 * @param <T>.
 */
public class Caja<T>{
    private T objeto;
    
    public Caja(){}//Constructor vacio
    
    public Caja(T objeto){
        this.objeto = objeto;
    }

    public T getObjeto() {
        return objeto;
    }

    public void setObjeto(T objeto) {
        this.objeto = objeto;
    }

    @Override
    public String toString() {
        return "Caja{" + "el objeto: "+ this.objeto+" es de tipo:" + this.objeto.getClass().getName() + '}';
    }
}