/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import Negocio.ColegioConjunto;

/**
 *
 * @author quasi201
 */
public class TestColegio {
    public static void main(String[] args) {
        try {
            ColegioConjunto myColegio =  new ColegioConjunto("UFPS-teen",5);
            Estudiante es1 = new Estudiante(1151902, "Marco");
            Estudiante es2 = new Estudiante(1151903, "felipe");
            Estudiante es3 = new Estudiante(1151904, "harold");
            Estudiante es4= new Estudiante(1151905, "oscar");
            Estudiante es5 = new Estudiante(1151906, "daniela");
            myColegio.matricular(es1);
            myColegio.matricular(es2);
            myColegio.matricular(es3);
            myColegio.matricular(es4);
            myColegio.matricular(es5);
            myColegio.desMatricular(es2);
            System.out.println(myColegio.toString()+"\n");
            System.out.println(myColegio.getListadoNombres());
            //System.out.println("El estudiante: "+es2.getNombre()+" esta en la posicion: "+myColegio.getDondeEsta(es2));
            
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        
        
    }
}
